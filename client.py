from twirp.context import Context
from twirp.exceptions import TwirpServerException

from pr_twirp import RateServiceClient
from pr_pb2 import Empty

client = RateServiceClient("http://localhost:3000")

# if you are using a custom prefix, then pass it as `server_path_prefix`
# param to `MakeHat` class.
try:
    response = client.GetDollarRate(ctx=Context(), request=Empty())
    print(response)
except TwirpServerException as e:
    print(e.code, e.message, e.meta, e.to_dict())
