from twirp.asgi import TwirpASGIApp
from twirp.exceptions import InvalidArgument
import aiohttp
import xml.etree.ElementTree as ET

from pr_twirp import RateServiceServer
from pr_pb2 import DollarRate


# if __name__ == "__main__":
#     pass


async def get_dollar_rate() -> float:
    url = 'http://www.cbr.ru/scripts/XML_daily.asp'
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as resp:
            data = await resp.text()
    tree = ET.fromstring(data)
    # print(tree)
    value = tree.findall('./Valute')[10].find('Value').text
    # row = tree.find("./ValCurs/Valute[@ID='R01235]/Value'")
    # print(row)
    prepared_value = round(float(value.replace(',', '.')), 2)
    print(prepared_value)
    return prepared_value


class RateService(object):
    async def GetDollarRate(self, context, empty):
        return DollarRate(
            value=await get_dollar_rate()
        )


service = RateServiceServer(service=RateService())
app = TwirpASGIApp()
app.add_service(service)


