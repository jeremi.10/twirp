FROM python:3.9
ADD . /code
WORKDIR /code
RUN pip install -r requirements.txt && apt-get install -y golang && apt-get install -y protobuf-compiler && go get -u github.com/verloop/twirpy/protoc-gen-twirpy
ENV PATH=$PATH:/home/ilia/go/bin/:/root/.local/bin:/home/ilia/.local/bin
ENV PROTOCOL_BUFFERS_PYTHON_IMPLEMENTATION=python
CMD python app.py
