import unittest
from app import get_dollar_rate


class TestCurrencyRequest(unittest.IsolatedAsyncioTestCase):

    async def test_get_dollar(self):
        assert 72.76 == await get_dollar_rate()


if __name__ == '__main__':
    unittest.main()
